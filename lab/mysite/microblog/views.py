from django.core.urlresolvers import reverse
from django.http import HttpResponseRedirect
from django.shortcuts import get_object_or_404
from django.contrib import messages

from models import BlogMessage

def add(request):
    if(request.method == 'POST'):
        title = request.POST.get('title', 0)
        message = request.POST.get('text', 0)
        nick = request.POST.get('author', 0)
        entry = BlogMessage(title=title,message = message, nick=nick)
        entry.full_clean()
        entry.save()
        url = reverse('message_list')
    else:
        url = reverse('entry_add')
    return HttpResponseRedirect(redirect_to=url)


