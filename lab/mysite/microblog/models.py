from django.db import models


# Create your models here.

class BlogMessage(models.Model):
    title = models.CharField(max_length=40)
    message = models.CharField(max_length=160)
    pub_date = models.DateTimeField(auto_now=True)
    nick = models.CharField(max_length=32)

    def __unicode__(self):
        return self.title