from django.test import TestCase
from models import *

class MicroblogTest(TestCase):

    def setUp(self):
        self.ex_title = "Test title"
        self.ex_message = "test text"
        self.ex_author = "test autor"
        self.message = BlogMessage.objects.create(
            title=self.ex_title,
            message=self.ex_message,
            nick=self.ex_author  )

    def test_message_display(self):
        self.assertEquals(unicode(self.message), self.ex_title)
        self.assertEquals(unicode(self.message.message), self.ex_message)
        self.assertEquals(unicode(self.message.title), self.ex_title)
        self.assertEquals(unicode(self.message.nick), self.ex_author)

    def test_add_message(self):
        url = '/blog/add/'


        title = 'Tyt'
        autor = 'Aut'
        text = 'text'

        # valid Form
        response = self.client.post(url, {'title': title,
                                          'message': text,
                                          'author': autor})
        urlresp = self.client.get('/blog', follow=True)
        print urlresp
        self.assertTrue(title in urlresp)

