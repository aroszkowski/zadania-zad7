from django.conf.urls import patterns, url
from django.views.generic import TemplateView, ListView, DetailView
from django.http import HttpResponse
from django.views.generic import ListView, DetailView
from microblog.models import BlogMessage
import views

urlpatterns = patterns('',
    #url(r'^$', TemplateView.as_view(template_name='microblog/stub.html')),
    url(r'^$', ListView.as_view(
        queryset=BlogMessage.objects.order_by('-pub_date'),
        context_object_name='blogmessages',
        template_name="microblog/list.html"
    ),
        name="message_list"),
    url(r'^add/',views.add, name = 'blog_add'),
    url(r'^addentry/',TemplateView.as_view(template_name='microblog/add.html'), name='entry_add')

)