from django.db import models
from django.contrib.auth.models import User


class Entry(models.Model):
    title = models.CharField(max_length=40)
    text = models.CharField(max_length=400)
    date = models.DateTimeField(auto_now=True)
    author = models.ForeignKey(User)

    def __unicode__(self):
        return self.title
